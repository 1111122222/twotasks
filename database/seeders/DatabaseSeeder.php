<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Country;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         Country::factory(6)->create()
         ->each(
             function ($country) {
                 Company::factory(2)->make()
                 ->each(
                     function ($company) use ($country){
                         $country->companies()->save($company);
                         User::factory(10)->make()->each(
                             function ($user) use ($company){
                                 $company->users()->save($user);
                             }
                         );
                     }
                 );
             }
         );


    }
}
