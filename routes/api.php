<?php

use App\Http\Controllers\CountryController;
use App\Http\Controllers\PdfFileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/country/{country}/users', [CountryController::class, 'fetchUsers']);
Route::get('/country/{country}/users-arranged', [CountryController::class, 'fetchUsersArrangedByCompany']);

Route::post('/pdf-upload/{needle_word?}', [PdfFileController::class, 'upload'])->middleware('pdf.only');
