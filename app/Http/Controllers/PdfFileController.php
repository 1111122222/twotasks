<?php

namespace App\Http\Controllers;

use App\Models\PdfFile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\PdfToText\Pdf;
use Symfony\Component\CssSelector\Exception\InternalErrorException;

class PdfFileController extends Controller
{
    /**
     *
     * @param Request $request
     * @param null $needle_word
     * @return JsonResponse
     */
    public function upload(Request $request, $needle_word=Null) : JsonResponse
    {
        try {
            $size = $request->file('file')->getSize();
            $name = $request->file('file')->getClientOriginalName();
            $text = Pdf::getText($request->file('file'));
            if (Str::contains(strtolower($text), $needle_word || config('twotasks.needle_word'))) {
                $pdf = PdfFile::updateOrCreate(
                    ['name' => $name, 'size' => $size],
                    ['link' => Storage::putFileAs( 'pdf', $request->file('file'), $name)]
                );
                $exists = $pdf->wasRecentlyCreated;
                return response()->json([], $exists ? 201 : 200);
            }else{
                abort(200, "Received, but it doesn't contain the required word");
            }
        } catch (InternalErrorException $e) {
            abort(400, "Please make sure this is a processable PDF");
        }
    }
}
