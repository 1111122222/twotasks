<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\Country;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\CssSelector\Exception\InternalErrorException;

class CountryController extends Controller
{
    /**
     * @param Request $request
     * @param $country
     * @return JsonResponse
     */
    public function fetchUsers(Request $request, $country): JsonResponse
    {
        try {
            $countryModel = Country::where('name', $country)->first();
            return response()->json($countryModel->users);
        } catch (InternalErrorException $e) {
            abort(500, "Something went wrong, kindly contact ....");
        }

    }

    /**
     * @param Request $request
     * @param $country
     * @return JsonResponse
     */
    public function fetchUsersArrangedByCompany(Request $request, $country): JsonResponse
    {
        try {
            $countryModel = Country::where('name', $country)->first();
            $companies = [];
            foreach ($countryModel->users as $user) {
                    $this->prepareCompanies($companies, $user->company, ['id', 'name', 'country_id']);
                    $companies[$user->company->name]['users'][] = new UserResource($user);
            }
            return response()->json([$countryModel->name => $companies]);
        } catch (InternalErrorException $e) {
            abort(500, "Something went wrong, kindly contact ....");
        }
    }

    /**
     * $array values must match the keys in the company array
     * @param $responseArray
     * @param $company
     * @param $array
     * @return mixed
     */
    private function prepareCompanies($responseArray, $company, $array){
        foreach($array as $value){
            $responseArray[$company->$value][$value] = $company->$value;
        }
        return $responseArray;
    }
}
