<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PdfOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Str::contains($request->header('Content-type'), 'multipart/form-data') ||
            !Str::contains($request->file('file')->getMimeType(), 'pdf')) {
            return response()->json([], 422);
        }

        return $next($request);
    }
}
