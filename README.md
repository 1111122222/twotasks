<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Before starting

run `composer i`, if not installed, [install it](https://getcomposer.org/download/)

Setup your .env to include the database details, then run

``` php artisan migrate ```

Following: run the following to seed the database

``` php artisan db:seed ```

And run ``` php artisan serve ``` to start.

# Relations Task

`TASK` Please create relationships, and a query that will get us all users who are associated with companies in a given country.

We will also need to display all company names the users associated with and dates when a user was associated with a company

> P.S: in these relations I have used an eloquent Model without hydration, as it is not required,
> adding to that, IMHO, I would prefer to do the below instead of writing DB::query()
> [Eloquent taking too long - StackOverflow](https://stackoverflow.com/a/48936062/9041947)

> Created 2 endpoints to show the data scattered by
> user that includes the company, and grouped by the company to include users under the country.

###API
```curl --location --request GET 'http://localhost:8000/api/country/USA/users' --header 'Content-Type: application/json'``` 

```asdfasdf```

# PDF Task

Behind the scenes this package leverages `pdftotext`. [for more info](https://github.com/spatie/pdf-to-text#requirements)

`REQUIRED` If the file is not PDF, return 422 error.

---------------------
``curl --location --request POST 'http://localhost:8000/api/pdf-upload' \
--form 'file=@"./dummyFile.png"' ``
---------------------
`REQUIRED` If the file doesn't contain the word "Proposal", just ignore the PDF.

-----------------------------
``curl --location --request POST 'http://localhost:8000/api/pdf-upload' \
--form 'file=@"./doesntContainProposal.png"' ``

checking the word with all variations.
Also, to change the file, go to `config/requestedword.php`
---------------------

`REQUIRED` If it's a PDF file, check if we already have the file with the same name and size (the `name` and `size` columns in our DB table)

---------------------
``curl --location --request POST 'http://localhost:8000/api/pdf-upload' \
--form 'file=@"/home/hamza/Documents/Hamza-Mostafa-010-69-00-00-63.pdf"'``
---------------------
`REQUIRED`  Upsert: If we have it, we'll need to update the existing one instead of creating a new record for the file.

---------------------
``curl --location --request POST 'http://localhost:8000/api/pdf-upload' \
--form 'file=@"/home/hamza/Documents/Hamza-Mostafa-010-69-00-00-63.pdf"'``
